package com.example.rozanovka.cnnlist.ui.list;


import com.example.rozanovka.cnnlist.R;
import com.example.rozanovka.cnnlist.ui.list.data.CNNArchitecture;
import com.example.rozanovka.cnnlist.ui.list.data.SQLiteHelper;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ListCNN extends ListActivity {

    private ListView obj;
    SQLiteHelper mydb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_list);

        mydb = new SQLiteHelper(this);
        List<CNNArchitecture> array_list = mydb.getAllCNN();
        ItemsAdapter adapter = new ItemsAdapter(this, android.R.layout.activity_list_item, array_list);
        setListAdapter(adapter);
        obj = findViewById(R.id.list_view);
        obj.setClickable(true);
        obj.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
                int id_To_Search = arg2 + 1;

                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", id_To_Search);
                Intent intent = new Intent(getApplicationContext(),DisplayCNN.class);

                intent.putExtras(dataBundle);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        int id_To_Search = position + 1;

        Bundle dataBundle = new Bundle();
        dataBundle.putInt("id", id_To_Search);
        Intent intent = new Intent(getApplicationContext(),DisplayCNN.class);

        intent.putExtras(dataBundle);
        startActivity(intent);

    }

    public String[] readLines(String filename) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getAssets().open(filename)));
            List<String> lines = new ArrayList<>();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            bufferedReader.close();
            return lines.toArray(new String[lines.size()]);
        }

    private class ItemsAdapter extends ArrayAdapter {

        private List<CNNArchitecture> items;
        private final Activity context;
        private SQLiteHelper mydb;

        ItemsAdapter(Activity context, int textViewResourceId, List<CNNArchitecture> items) {
            super(context, textViewResourceId, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public View getView(final int position, @Nullable View rowView, @NonNull ViewGroup parent) {
            mydb = new SQLiteHelper(this.context);

            ViewHolder viewHolder;


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.row_view, parent, false);
            viewHolder.txtName = rowView.findViewById(R.id.name);
            viewHolder.icon = rowView.findViewById(R.id.icon);
            viewHolder.deleteBtn = rowView.findViewById(R.id.delete_btn);
            String name = items.get(position).getName();
            String imgName = items.get(position).getImageName();

            viewHolder.txtName.setText(name);
            try
            {
                InputStream ims = getAssets().open(imgName);
                Drawable d = Drawable.createFromStream(ims, null);
                viewHolder.icon.setImageDrawable(d);
                ims .close();
            }
            catch(IOException ex)
            {

            }

            viewHolder.deleteBtn.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    items.remove(position);
                    mydb.deleteCNN(items.get(position).getId());
                    notifyDataSetChanged();
                }
            });

            return rowView;
        }

        private class ViewHolder {

            TextView txtName;
            ImageView icon;
            ImageView deleteBtn;

        }

    }

}

