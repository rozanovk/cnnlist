package com.example.rozanovka.cnnlist.ui.list;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rozanovka.cnnlist.R;
import com.example.rozanovka.cnnlist.ui.list.data.SQLiteHelper;

import java.io.IOException;
import java.io.InputStream;

public class DisplayCNN extends Activity {
    TextView name;
    TextView description;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_cnn);
        name = findViewById(R.id.textViewName);
        description = findViewById(R.id.textViewDescription);
        image = findViewById(R.id.imageView);

        SQLiteHelper mydb = new SQLiteHelper(this);

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            int Value = extras.getInt("id");

            if(Value>0){
                Cursor rs = mydb.getData(Value);
                rs.moveToFirst();

                String nam = rs.getString(rs.getColumnIndex(SQLiteHelper.CNN_NAME_COLUMN));
                String desc = rs.getString(rs.getColumnIndex(SQLiteHelper.CNN_DESCRIPTION_COLUMN));
                String imgName = rs.getString(rs.getColumnIndex(SQLiteHelper.CNN_IMAGENAME_COLUMN));

                if (!rs.isClosed())  {
                    rs.close();
                }

                name.setText(nam);
                name.setFocusable(false);
                name.setClickable(false);

                description.setText(desc);
                description.setFocusable(false);
                description.setClickable(false);

                try
                {
                    // get input stream
                    InputStream ims = getAssets().open(imgName);
                    // load image as Drawable
                    Drawable d = Drawable.createFromStream(ims, null);
                    // set image to ImageView
                    image.setImageDrawable(d);
                    ims .close();
                }
                catch(IOException ex)
                {
                    return;
                }

            }
        }
    }

}
