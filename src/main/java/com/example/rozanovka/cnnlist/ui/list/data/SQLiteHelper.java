package com.example.rozanovka.cnnlist.ui.list.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "cnn.db";
    private static final String TABLE_NAME = "cnn";
    public static final String CNN_ID_COLUMN = "id";
    public static final String CNN_NAME_COLUMN = "name";
    public static final String CNN_DESCRIPTION_COLUMN = "description";
    public static final String CNN_IMAGENAME_COLUMN = "imageName";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (!isTableExists(db)){
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS cnn (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            ""+CNN_NAME_COLUMN+" varchar(255), "+CNN_DESCRIPTION_COLUMN+" varchar(255)," +
                            " "+CNN_IMAGENAME_COLUMN+" varchar(255))"
            );
            insertCNN("LeNet", "LeNet-5, a pioneering 7-level " +
                "convolutional network by LeCun et al in 1998, that classifies digits, " +
                "was applied by several banks to recognise hand-written numbers on checks (cheques) " +
                "digitized in 32x32 pixel greyscale inputimages. ",
                "lenet_logo.png", db);

            insertCNN("ZFNet", "ILSVRC 2013 winner. " +
                        "It achieved a top-5 error rate of 14.8% which is now already half of the " +
                        "prior mentioned non-neural error rate. It was mostly an achievement by " +
                        "tweaking the hyper-parameters of AlexNet while maintaining the same structure " +
                        "with additional Deep Learning elements as discussed earlier in this essay. ",
                "zfnet_logo.png", db);

            insertCNN("VGGNet", " ILSVRC 2014 competition is dubbed " +
                        "VGGNet by the community and was developed by Simonyan and Zisserman . " +
                        "VGGNet consists of 16 convolutional layers and is very appealing because of " +
                        "its very uniform architecture. Similar to AlexNet, only 3x3 convolutions, " +
                        "but lots of filters. Trained on 4 GPUs for 2–3 weeks",
                    "vgg_logo.png", db);

            insertCNN("ResNet", "ILSVRC 2015 winner was " +
                        "Residual Neural Network (ResNet) by Kaiming He et al introduced anovel " +
                        "architecture with “skip connections” and features heavy batch normalization",
                "resnet_logo.png", db);


        }else{
            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS cnn (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            ""+CNN_NAME_COLUMN+" varchar(255), "+CNN_DESCRIPTION_COLUMN+" varchar(255)," +
                            " "+CNN_IMAGENAME_COLUMN+" varchar(255))"
        );
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS cnn");
        onCreate(db);
    }

    private void insertCNN(String name, String description, String imageName,SQLiteDatabase db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CNN_NAME_COLUMN, name);
        contentValues.put(CNN_DESCRIPTION_COLUMN, description);
        contentValues.put(CNN_IMAGENAME_COLUMN, imageName);
        db.insert("cnn", null, contentValues);
    }

    public void deleteCNN(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        db.rawQuery("delete from "+TABLE_NAME+" where "+CNN_ID_COLUMN+"="+id+"", null);
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME+" where " +
                ""+CNN_ID_COLUMN+"="+id+"", null );
        return res;
    }

    public List<CNNArchitecture> getAllCNN() {
        List<CNNArchitecture> array_list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME+"", null );
        res.moveToFirst();

        while(!res.isAfterLast()){
            CNNArchitecture obj = new CNNArchitecture();
            obj.setId(res.getInt(res.getColumnIndex("id")));
            obj.setName(res.getString(res.getColumnIndex("name")));
            obj.setDescription(res.getString(res.getColumnIndex("description")));
            obj.setImageName(res.getString(res.getColumnIndex("imageName")));
            array_list.add(obj);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    private boolean isTableExists(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select DISTINCT "+TABLE_NAME+" from sqlite_master " +
                "where tbl_name = '"+TABLE_NAME+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

}



